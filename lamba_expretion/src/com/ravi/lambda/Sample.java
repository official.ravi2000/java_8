package com.ravi.lambda;

public class Sample {
	public static void main(String[] args) {
		/*
		 * Thread thread = new Thread(new Runnable() {
		 * 
		 * @Override public void run() { System.out.println("In another Thread");
		 * 
		 * } });
		 * 
		 */

		Thread thread;
		thread= new Thread(() -> System.out.println("In another Thread"));
		thread= new Thread(() -> System.out.println("In another Thread"));
		thread= new Thread(() -> System.out.println("In another Thread"));
		thread= new Thread(() -> System.out.println("In another Thread"));
		
		thread.start();

		System.out.println("In main method");
	}
}
