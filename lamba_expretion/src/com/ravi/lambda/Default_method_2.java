package com.ravi.lambda;

interface Left{
	default String myMethod() {
		return "Left interface Default Method ";
	}
}

interface Right{
	default String myMethod() {
		return "Right interface Default Method ";
	}
}

public class Default_method_2 implements Left,Right{
	
	public static void main(String[] args) {
		System.out.println(new Default_method_2().myMethod());
	}

	@Override
	public String myMethod() {
		//return Left.super.myMethod();
		return Right.super.myMethod();
	}
}
