package com.ravi.lambda;

interface myInterFace5{
	default String myMethod() {
		return "Static Method ";
	}
}


public class Static_Method implements myInterFace5{
	public static void main(String[] args) {
		Static_Method demo = new Static_Method();
		demo.myMethod();
	}
}
