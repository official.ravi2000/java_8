package com.ravi.lambda;

class MyRunnable implements Runnable {
	@Override
	public void run() {
		for (int i = 1; i < 10; i++) {
			System.out.println("Child Thread " + i);
		}

	}
}

public class Lamba_expression_2 {
	public static void main(String[] args) {
		// Runnable myRunnable = new MyRunnable();
		// Thread thread = new Thread(myRunnable);

		Thread thread = new Thread(() -> {
			for (int i = 1; i < 10; i++) {
				System.out.println("Child Thread " + i);
			}
		});

		thread.start();

		for (int i = 0; i < 10; i++) {
			System.out.println("Main Thread " + i);
		}
	}
}
