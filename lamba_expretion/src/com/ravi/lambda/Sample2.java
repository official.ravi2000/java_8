package com.ravi.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Sample2 {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0);

//		for (int i = 0; i < list.size(); i++) {
//			System.out.println(list.get(i));
//		}

		// external iterator
//		for(int number : list) {
//			System.out.println(number);
//		}

		// internal iterator
//		list.forEach(new Consumer<Integer>() {
//			@Override
//			public void accept(Integer value) {
//				System.out.println(value);
//				
//			}
//		});

		
		//list.forEach((Integer value) -> System.out.println(value));
		
		//Parenthesis is option but only for single parameter lambdas
		list.forEach(value -> System.out.println(value));
		
		//method reference syntax
		list.forEach(System.out::println);
	}
}
