package com.ravi.lambda;

public class anonymous_vs_lamda {
	
	/**
	 * with Anonymous inner class
	 */
//	public static void main(String[] args) {
//		Thread thread = new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				System.out.println("with anonyous inner class");
//
//			}
//		});
//		
//		thread.start();
//		
//	}
	
	public static void main(String[] args) {
		Thread thread = new Thread(()->{
			System.out.println("With Lambda Expression");
		});
	}
	
	
	
}
