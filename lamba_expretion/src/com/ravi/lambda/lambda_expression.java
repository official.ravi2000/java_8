package com.ravi.lambda;

interface MyInterFace {
	public int sum(int a, int b);
}

interface MyInterFace1 {
	public int square(int x);
}

class Demo implements MyInterFace {

	@Override
	public int sum(int a, int b) {
		return a + b;

	}

}

public class lambda_expression {
	public static void main(String[] args) {
		Demo demo = new Demo();
		System.out.println("Sum is " + demo.sum(10, 20));

		MyInterFace result = ((a, b) -> {
			return a + b;
		});

		System.out.println(result.sum(10, 20));

		MyInterFace1 interFace1 = x -> x * x;

		System.out.println("Square of a number " + interFace1.square(5));

	}
}
